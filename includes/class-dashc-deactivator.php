<?php

/**
 * Fired during plugin deactivation
 *
 * @link       Dashboard-Cleanup
 * @since      1.0.0
 *
 * @package    Dashc
 * @subpackage Dashc/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Dashc
 * @subpackage Dashc/includes
 * @author     Austin Vern Songer <austinvernsonger@protonmail.com>
 */
class Dashc_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
