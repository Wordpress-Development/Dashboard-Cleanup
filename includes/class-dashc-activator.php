<?php

/**
 * Fired during plugin activation
 *
 * @link       Dashboard-Cleanup
 * @since      1.0.0
 *
 * @package    Dashc
 * @subpackage Dashc/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Dashc
 * @subpackage Dashc/includes
 * @author     Austin Vern Songer <austinvernsonger@protonmail.com>
 */
class Dashc_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
